JAVAC = javac
SRCDIR = src
BUILDDIR = build

SOURCES := $(shell find $(SRCDIR) -name '*.java')

CLASSPATH = $(BUILDDIR)

JAVACFLAGS = -d $(BUILDDIR) -cp $(CLASSPATH)

MAINCLASS = Main
default: build

build: $(SOURCES)
	@mkdir -p $(BUILDDIR)
	$(JAVAC) $(JAVACFLAGS) $(SOURCES)

run: build
	java -cp $(CLASSPATH) $(MAINCLASS)
clean:
	rm -rf $(BUILDDIR)

.PHONY: build clean