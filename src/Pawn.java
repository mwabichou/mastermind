import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.*;
import javax.swing.*;
import java.io.Serializable;

class Pawn implements Serializable {
    JButton button;
    private int currentColorIndex = 0;
    private int nbColors;
    private boolean isClickable = true;

    public Pawn(int colors) {
        this.nbColors = colors;
    }

    public void setButton() {
        this.button = new JButton();
        button.setFont(new Font("Helvetica", Font.BOLD, 30));
        button.setOpaque(true);
        button.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        button.setBackground(Color.lightGray);
        button.setRolloverEnabled(true);
        setMouseListener();
    }

    public Color getCurrentColor() {
        switch (currentColorIndex) {
            case 1:
                return Color.red;
            case 2:
                return Color.cyan;
            case 3:
                return Color.green;
            case 4:
                return Color.yellow;
            case 5:
                return Color.orange;

            case 6:
                return Color.pink;
            case 7:
                return Color.blue;
            case 8:
                return Color.magenta;
            default:
                return Color.lightGray;
        }
    }

    public int getCurrentColorIndex() {
        return currentColorIndex;
    }

    public void setCurrentColorIndex(int currentColorIndex) {
        this.currentColorIndex = currentColorIndex;
    }

    public void disableButton() {
        this.button.setEnabled(false);
    }

    public void setButtonClickable(boolean clickable) {
        this.isClickable = clickable;
    }

    public void setIconColor(int color) {
        switch (color) {
            case 1:
                button.setIcon(new ImageIcon("src/img/black-circle.png"));
                break;
            case 0:
                button.setIcon(new ImageIcon("src/img/white-circle.png"));
                break;
            default:
                break;
        }
    }

    public void setColor(int color) {
        currentColorIndex = color;
        button.setBackground(getCurrentColor());
        if (currentColorIndex == 0) {
            button.setText("");
        } else {
            button.setText("" + getCurrentColorIndex());
        }
    }

    public void updatePawn() {
        button.setBackground(getCurrentColor());
        if (currentColorIndex == 0) {
            button.setText("");
        } else {
            button.setText("" + getCurrentColorIndex());
        }
        setMouseListener();
    }

    @Override
    public String toString() {
        String circle = "\u2B24";

        switch (currentColorIndex) {
            case 1:
                return "\u001B[31m" + circle;
            case 2:
                return "\u001B[36m" + circle;
            case 3:
                return "\u001B[32m" + circle;
            case 4:
                return "\u001B[33m" + circle;
            case 5:
                return "\u001B[35m" + circle;
            case 6:
                return "\u001B[35m" + circle;
            case 7:
                return "\u001B[34m" + circle;
            case 8:
                return "\u001B[35m" + circle;
            default:
                return "\u001B[0m" + circle;
        }
    }

    public void setMouseListener() {
        for (MouseListener ml : button.getMouseListeners()) {
            button.removeMouseListener(ml);
        }
        button.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (isClickable) {
                        currentColorIndex = (currentColorIndex + 1) % (nbColors + 1);
                        button.setBackground(getCurrentColor());
                        if (currentColorIndex == 0) {
                            button.setText("");
                        } else {
                            button.setText("" + getCurrentColorIndex());
                        }
                    }
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    if (isClickable) {
                        currentColorIndex = (currentColorIndex + nbColors) % (nbColors + 1);
                        button.setBackground(getCurrentColor());
                        if (currentColorIndex == 0) {
                            button.setText("");
                        } else {
                            button.setText("" + getCurrentColorIndex());
                        }
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                if (isClickable && button.getBackground() == Color.lightGray) {
                    button.setBackground(Color.white);
                    ;
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (isClickable) {
                    button.setBackground(getCurrentColor());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });
    }
}
