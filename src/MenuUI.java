import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.io.File;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;

class MenuUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private ImageIcon playIcon = new ImageIcon("src/img/play.png");
    // private ImageIcon optionsIcon = new ImageIcon("src/img/book.png");
    private ImageIcon exitIcon = new ImageIcon("src/img/exit.png");
    private ImageIcon terminalIcon = new ImageIcon("src/img/terminal.png");
    private ImageIcon logoIcon = new ImageIcon("src/img/MM_logo2.png");
    private ImageIcon loadIcon = new ImageIcon("src/img/load.png");
    private ImageIcon backgroundIcon = new ImageIcon("src/img/background.png");

    int nbPlayers = 1;
    int pawns = 4;
    int attemps = 10;
    int colors = 6;
    boolean repeatColors = false;
    int nbGames = 1;

    public MenuUI() {
        super("MasterMind | Menu");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(600, 800));
        setLocationRelativeTo(null);
        setResizable(true);
        setIconImage(logoIcon.getImage());
        setVisible(true);
        this.setContentPane(new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundIcon.getImage(), 0, 0, 1300, 800, null);
            }
        });
        int buttonWidth = getWidth() / 2;
        int buttonHeight = getHeight() / 10;
        int buttonSpacing = getHeight() / 30;
        setLayout(null);
        JLabel logo = new JLabel();
        logo.setIcon(logoIcon);
        logo.setBounds((getWidth() - logoIcon.getIconWidth()) / 2, 40, logoIcon.getIconWidth(),
                logoIcon.getIconHeight());
        add(logo);

        JButton new_game = new JButton("Play Game");
        new_game.setFont(new Font("Helvetica", Font.BOLD, 35));
        new_game.setOpaque(true);
        new_game.setBackground(new Color(0, 0, 0, 50));
        new_game.setIcon(playIcon);
        new_game.setHorizontalTextPosition(SwingConstants.LEFT);
        new_game.setForeground(Color.white);
        new_game.setBorder(BorderFactory.createLineBorder(Color.white));
        new_game.setRolloverEnabled(true);
        new_game.setAlignmentX(Component.CENTER_ALIGNMENT);
        new_game.setAlignmentY(Component.CENTER_ALIGNMENT);
        new_game.setBounds((getWidth() - buttonWidth) / 2, getHeight() / 4, buttonWidth, buttonHeight);
        new_game.setContentAreaFilled(false);

        JButton loadBtn = new JButton("Load Game");
        loadBtn.setFont(new Font("Helvetica", Font.BOLD, 35));
        loadBtn.setOpaque(true);
        loadBtn.setBackground(new Color(0, 0, 0, 50));
        loadBtn.setIcon(loadIcon);
        loadBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        loadBtn.setForeground(Color.white);
        loadBtn.setBorder(BorderFactory.createLineBorder(Color.white));
        loadBtn.setRolloverEnabled(true);
        loadBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        loadBtn.setAlignmentY(Component.CENTER_ALIGNMENT);
        loadBtn.setBounds((getWidth() - buttonWidth) / 2, new_game.getY() + buttonHeight + buttonSpacing, buttonWidth,
                buttonHeight);

        loadBtn.setContentAreaFilled(false);
        JButton helpBtn = new JButton("Report !");
        helpBtn.setFont(new Font("Helvetica", Font.BOLD, 35));
        helpBtn.setOpaque(true);
        helpBtn.setForeground(Color.white);
        // helpBtn.setIcon(optionsIcon);
        helpBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        helpBtn.setBackground(new Color(0, 0, 0, 50));
        helpBtn.setBorder(BorderFactory.createLineBorder(Color.white));
        helpBtn.setRolloverEnabled(true);
        helpBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        helpBtn.setAlignmentY(Component.CENTER_ALIGNMENT);

        helpBtn.setBounds((getWidth() - buttonWidth) / 2, loadBtn.getY() + buttonHeight + buttonSpacing, buttonWidth,
                buttonHeight);
        helpBtn.setContentAreaFilled(false);
        JButton terminal_mode = new JButton("TerminalUI");
        terminal_mode.setFont(new Font("Helvetica", Font.BOLD, 35));
        terminal_mode.setOpaque(true);
        terminal_mode.setIcon(terminalIcon);
        terminal_mode.setHorizontalTextPosition(SwingConstants.LEFT);
        terminal_mode.setBackground(new Color(0, 0, 0, 50));
        terminal_mode.setForeground(Color.white);
        terminal_mode.setBorder(BorderFactory.createLineBorder(Color.white));
        terminal_mode.setRolloverEnabled(true);
        terminal_mode.setAlignmentX(Component.CENTER_ALIGNMENT);
        terminal_mode.setAlignmentY(Component.CENTER_ALIGNMENT);
        terminal_mode.setBounds((getWidth() - buttonWidth) / 2, helpBtn.getY() + buttonHeight + buttonSpacing,
                buttonWidth, buttonHeight);

        terminal_mode.setContentAreaFilled(false);
        JButton exitBtn = new JButton("Exit");
        exitBtn.setFont(new Font("Helvetica", Font.BOLD, 35));
        exitBtn.setOpaque(true);
        exitBtn.setBackground(new Color(0, 0, 0, 50));
        exitBtn.setForeground(Color.white);
        exitBtn.setIcon(exitIcon);
        exitBtn.setHorizontalTextPosition(SwingConstants.LEFT);
        exitBtn.setBorder(BorderFactory.createLineBorder(Color.white));
        exitBtn.setRolloverEnabled(true);
        exitBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitBtn.setAlignmentY(Component.CENTER_ALIGNMENT);
        exitBtn.setBounds((getWidth() - buttonWidth) / 2, terminal_mode.getY() + buttonHeight + buttonSpacing,
                buttonWidth, buttonHeight);
        exitBtn.setContentAreaFilled(false);
        exitBtn.addActionListener(e -> System.exit(0));
        JLabel text = new JLabel("Coded by Wassim ...");
        text.setFont(new Font("Helvetica", Font.BOLD, 20));
        text.setForeground(Color.white);
        int textWidth = 200;
        int textHeight = 50;
        text.setBounds((getWidth() - textWidth) / 2, getHeight() - textHeight - 20, textWidth, textHeight);
        add(text);
        loadBtn.addActionListener(e -> loadFile());
        new_game.addActionListener(e -> {
            options();
            dispose();
        });
        helpBtn.addActionListener(e -> {
            openPdf("mastermind.pdf");
        });
        terminal_mode.addActionListener(e -> terminal_mode());
        add(new_game);
        add(loadBtn);
        add(terminal_mode);
        add(helpBtn);
        add(exitBtn);
        pack();

    }

    private void loadFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("MasterMind | Load");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setCurrentDirectory(new File("src/save"));
        int userSelection = fileChooser.showOpenDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            new Window().load(selectedFile.toPath().toString());
        }
    }

    private static void options() {
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Helvetica", Font.BOLD, 20)));
        UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("Helvetica", Font.BOLD, 20)));
        UIManager.put("OptionPane.minimumSize", new Dimension(500, 200));
        UIManager.put("OptionPane.background", new Color(172, 133, 174));
        UIManager.put("Panel.background", new Color(172, 133, 174));
        UIManager.put("OptionPane.messageForeground", Color.white);
        UIManager.put("OptionPane.messageAlignment", SwingConstants.CENTER);
        ImageIcon icon = new ImageIcon("src/img/setting.png");
        JOptionPane optionPane = new JOptionPane();
        optionPane.setIcon(icon);
        optionPane.setMessageType(JOptionPane.QUESTION_MESSAGE);
        optionPane.setOptionType(JOptionPane.DEFAULT_OPTION);

        Object[] options = { "1", "2", "Bot", "Fast Game" };
        String player1 = "Wassim";
        String player2 = "Le Prof";
        optionPane.setMessage("Choose the number of players");
        optionPane.setOptions(options);
        // if fast game is selected
        int nbPlayers = JOptionPane.showOptionDialog(null, optionPane.getMessage(), "MasterMind | Options",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, options, options[3]);
        if (nbPlayers == 1) {
            player1 = JOptionPane.showInputDialog(null, "Enter player 1 name", "MasterMind | Options",
                    JOptionPane.QUESTION_MESSAGE);
            player2 = JOptionPane.showInputDialog(null, "Enter player 2 name", "MasterMind | Options",
                    JOptionPane.QUESTION_MESSAGE);
        } else if (nbPlayers == 3) {
            new Window(new GameBoard(5, 10, 6, false, new GridLayout(10, 5, 5, 5)), 800, 900,
                    new Player[] { new Player("Wassim") });
            return;
        }
        Object[] pawnsOptions = { "5", "4" };
        optionPane.setMessage("Choose the number of pawns");
        optionPane.setOptions(pawnsOptions);
        int pawns = JOptionPane.showOptionDialog(null, optionPane.getMessage(), "MasterMind | Options",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, pawnsOptions, pawnsOptions[0]);

        Object[] attempsOptions = { "10", "11", "12" };
        optionPane.setMessage("Choose the number of attemps");
        optionPane.setOptions(attempsOptions);
        int attemps = JOptionPane.showOptionDialog(null, optionPane.getMessage(), "MasterMind | Options",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, attempsOptions, attempsOptions[0]);

        Object[] colorsOptions = { "6", "7", "8" };
        optionPane.setMessage("Choose the number of colors");
        optionPane.setOptions(colorsOptions);
        int colors = JOptionPane.showOptionDialog(null, optionPane.getMessage(), "MasterMind | Options",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, colorsOptions, colorsOptions[0]);

        Object[] repeatColorsOptions = { "No", "Yes" };
        optionPane.setMessage("Repeat colors?");
        optionPane.setOptions(repeatColorsOptions);
        int repeatColors = JOptionPane.showOptionDialog(null, optionPane.getMessage(), "MasterMind | Options",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, icon, repeatColorsOptions,
                repeatColorsOptions[0]);

        nbPlayers = nbPlayers == 2 ? 3 : Integer.parseInt((String) options[nbPlayers]);
        pawns = Integer.parseInt((String) pawnsOptions[pawns]);
        attemps = Integer.parseInt((String) attempsOptions[attemps]);
        colors = Integer.parseInt((String) colorsOptions[colors]);
        boolean repeat = repeatColors == 1 ? true : false;
        Player[] players;
        switch (nbPlayers) {
            case 3:
                players = new Player[2];
                players[0] = new Player(player1);
                players[1] = new AutoPlayer();
                break;
            case 1:
                players = new Player[1];
                players[0] = new Player(player1);
                break;
            default:
                players = new Player[2];
                players[0] = new Player(player1);
                players[1] = new Player(player2);
        }

        int[] size = screenSize(pawns, attemps);
        new Window(new GameBoard(pawns, attemps, colors, repeat, new GridLayout(attemps, pawns, 5, 5)), size[0],
                size[1],
                players);
    }

    public static int[] screenSize(int pawns, int attemps) {
        if (pawns == 4) {
            if (attemps == 10) {
                return new int[] { 700, 900 };
            }
            if (attemps == 11) {
                return new int[] { 700, 950 };
            }
            if (attemps == 12) {
                return new int[] { 700, 950 };
            }
        }
        if (pawns == 5) {
            if (attemps == 10) {
                return new int[] { 800, 900 };
            }
            if (attemps == 11) {
                return new int[] { 800, 950 };
            }
            if (attemps == 12) {
                return new int[] { 800, 950 };
            }
        }
        return new int[] { 800, 900 };
    }

    private void terminal_mode() {
        dispose();
        new TerminalMode().init();
    }

    public static void openPdf(String path) {
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(new File(path));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
