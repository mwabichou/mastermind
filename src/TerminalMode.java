import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

class TerminalMode {

    GameBoard gameBoard;
    int nbgames;
    private HashMap<Integer, String> colorsMap = new HashMap<Integer, String>();
    private Player[] players;
    private int pawns = 5;
    private int attemps = 10;
    private int colors = 6;
    private boolean repeatColors = false;
    private AttempResult result;
    private int actualPlayer;
    private int nbPlayers;

    public TerminalMode() {
        initHashMap();
        init();
    }

    public void init() {
        logoASCII();
        try (Scanner in = new Scanner(System.in)) {
            System.out.println("Load or New game? (L/N) | F to fast play");
            String input = in.next();
            if (input.equals("l")) {
                System.out.println("Enter path to save file");
                String path = in.next();
                System.out.println(path);
                loadGame(path);
            } else if (input.equals("n")) {
                System.out.println("Number of Pawns");
                pawns = in.nextInt();
                System.out.println("Number of Attemps");
                attemps = in.nextInt();
                System.out.println("Number of Colors");
                colors = in.nextInt();
                System.out.println("Repeat Colors? (true/false)");
                repeatColors = in.nextBoolean();
                System.out.println("Number of games");
                nbgames = in.nextInt();
                System.out.println("Number of players : ");
                int n = in.nextInt();
                initPlayers(n);
                gameBoard = new GameBoard(pawns, attemps, colors, repeatColors);
            } else if (input.equals("f") || input.equals("F")) {
                initPlayers(1);
                gameBoard = new GameBoard(pawns, attemps, colors, repeatColors);
                nbgames = 2;
            }
            clearConsole();
            while (nbgames > 0) {
                play(gameBoard);
                nbgames--;
            }
        }

    }

    private void play(GameBoard gameBoard) {
        boolean isOver = false;
        actualPlayer = 0;
        nbPlayers = players.length;
        System.out.println("Game ");
        while (!isOver) {
            Scanner in = new Scanner(System.in);
            int[] guess = new int[gameBoard.getPawns()];
            logoASCII();
            gameBoard.printGameBoard();
            if (gameBoard.getCurrentAttemp() > 0) {
                result = gameBoard.checkRowFeedback(gameBoard.getCurrentAttemp() - 1);
                resultToBW(result);
            }
            showColorNumber(gameBoard.getColors());
            showPlayersScore();
            System.out.println();
            System.out.println("S: Save game | Q: Quit");
            System.out.println(players[actualPlayer].getName() + " :Enter your guess :");
            if (in.hasNextInt()) {
                for (int i = 0; i < gameBoard.getPawns(); i++) {
                    guess[i] = in.nextInt();
                }
                gameBoard.addGuess(guess);
                result = gameBoard.checkRowFeedback(gameBoard.getCurrentAttemp());
                players[actualPlayer].addScore(gameBoard.getAttemps() - gameBoard.getCurrentAttemp());
                actualPlayer = (actualPlayer + 1) % nbPlayers;
                gameBoard.increamentCurrentAttemp();
                if (checkWin()) {
                    gameBoard.printGameBoard();
                    System.out.println(players[actualPlayer].getName() + " wins!");
                    isOver = true;
                } else if (maxAttemps()) {
                    gameBoard.printGameBoard();
                    System.out.println("You lost!");
                    isOver = true;
                } else {
                    clearConsole();
                }
            } else {
                String input = in.next();
                if (input.equals("S")) {
                    System.out.println("Enter path to save file");
                    String path = in.next();
                    saveGame(path);
                } else if (input.equals("Q")) {
                    bestPlayer();
                    System.exit(0);
                }
            }
        }
        if (isOver && nbgames - 1 > 0) {
            System.out.println("Next Game? (Y/N)");
            Scanner in = new Scanner(System.in);
            String input = in.next();
            if (input.equals("Y") || input.equals("y")) {
                gameBoard.reset();
            } else {
                System.exit(0);
            }
        } else {
            System.exit(0);
        }
    }

    private void initPlayers(int n) {
        players = new Player[n];
        for (int i = 1; i <= n; i++) {
            players[i - 1] = new Player("P" + i);
        }
    }

    private boolean checkWin() {
        if (result.getCcAp() == gameBoard.getPawns()) {
            return true;
        }
        return false;
    }

    private void bestPlayer() {
        int max = 0;
        int index = 0;
        for (int i = 0; i < nbPlayers; i++) {
            if (players[i].getScore() > max) {
                max = players[i].getScore();
                index = i;
            }
        }
        System.out.println(players[index].getName() + " best Score " + " with " + max + " points");
    }

    private void logoASCII() {
        System.out.print(
                "\033[0;31m      <-. (`-')   (`-')  _  (`-').->(`-')      (`-')  _   (`-') <-. (`-')    _     <-. (`-')_  _(`-')    \n");
        System.out.print(
                "\033[0;34m        \\(OO )_  (OO ).-/  ( OO)_  ( OO).->   ( OO).-/<-.(OO )    \\(OO )_  (_)       \\( OO) )( (OO ).-> \n");
        System.out.print(
                "\033[0;32m     ,--./  ,--. / ,---.  (_)--\\_) /    '._  (,------.,------,),--./  ,--. ,-(`-'),--./ ,--/  \\    .'_  \n");
        System.out.print(
                "\033[0;33m     |   `.'   | | \\ /`.\\ /    _ / |'--...__) |  .---'|   /`. '|   `.'   | | ( OO)|   \\ |  |  '`'-..__) \n");
        System.out.print(
                "\033[0;36m     |  |'.'|  | '-'|_.' |\\_..`--. `--.  .--'(|  '--. |  |_.' ||  |'.'|  | |  |  )|  . '|  |) |  |  ' | \n");
        System.out.print(
                "\033[0;35m     |  |   |  |(|  .-.  |.-._)   \\   |  |    |  .--' |  .   .'|  |   |  |(|  |_/ |  |\\    |  |  |  / : \n");
        System.out.print(
                "\033[0;36m     |  |   |  | |  | |  |\\       /   |  |    |  `---.|  |\\  \\ |  |   |  | |  |'->|  | \\   |  |  '-'  / \n");
        System.out.print(
                "\033[0;34m     `--'   `--' `--' `--' `-----'    `--'    `------'`--' '--'`--'   `--' `--'   `--'  `--'  `------'  \n");
        System.out.print("\033[0m");

    }

    private boolean maxAttemps() {
        if (gameBoard.getCurrentAttemp() == gameBoard.getAttemps()) {
            return true;
        }
        return false;
    }

    public static void clearConsole() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                System.out.print("\033\143");
            }
        } catch (IOException | InterruptedException ex) {
        }
    }

    public void initHashMap() {
        String circle = "\u2B24";
        colorsMap.put(0, "\u001B[0m" + circle);
        colorsMap.put(1, "\u001B[31m" + circle);
        colorsMap.put(2, "\u001B[32m" + circle);
        colorsMap.put(3, "\u001B[33m" + circle);
        colorsMap.put(4, "\u001B[34m" + circle);
        colorsMap.put(5, "\u001B[35m" + circle);
        colorsMap.put(6, "\u001B[36m" + circle);
        colorsMap.put(7, "\u001B[32;1m" + circle);
        colorsMap.put(8, "\u001B[90m" + circle);

    }

    public void showColorNumber(int Colors) {
        System.err.println();
        for (int i = 1; i <= Colors; i++) {
            System.out.print(colorsMap.get(i) + " " + i + " ");
        }
        System.out.println();
    }

    private void resultToBW(AttempResult result) {
        String circle = "\u2B24";
        System.err.println();
        for (int i = 0; i < result.getCcAp(); i++) {
            System.out.print("\u001B[90m" + circle + "   ");
        }
        for (int i = 0; i < result.getCcWp(); i++) {
            System.out.print("\u001B[37m" + circle + "   ");
        }
        System.out.println();
    }

    private void showPlayersScore() {
        for (int i = 0; i < nbPlayers; i++) {
            System.out.println(players[i].getName() + " : " + players[i].getScore());
        }
        System.out.println();
    }

    public void saveGame(String path) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path))) {
            out.writeObject(gameBoard);
            out.writeObject(players);
            out.writeInt(actualPlayer);
            out.writeInt(nbgames);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadGame(String path) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(path))) {
            gameBoard = (GameBoard) in.readObject();
            players = (Player[]) in.readObject();
            actualPlayer = in.readInt();
            nbgames = in.readInt();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
