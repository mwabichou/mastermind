import java.awt.*;
import java.io.Serializable;
import javax.swing.*;
import java.util.Random;

public class GameBoard implements Serializable {
    private int pawns;
    private int attemps;
    int currentAttemp;
    private int colors;
    private boolean repeatColors;
    private Pawn[][] GameBoardOfPawns;
    private int[] secretCode;
    public JPanel panel;

    public GameBoard(int pawns, int attemps, int colors, boolean repeatColors) {
        if (pawns < 4 || pawns > 5 || attemps < 10 || attemps > 12 || colors < 6 ||
                colors > 8) {
            throw new IllegalArgumentException("Invalid game parameters");
        }
        this.pawns = pawns;
        this.attemps = attemps;
        this.colors = colors;
        this.currentAttemp = 0;
        this.repeatColors = repeatColors;
        this.GameBoardOfPawns = new Pawn[attemps][pawns];
        this.secretCode = new int[colors];
        initGameBoard();
        initSecretCode();
    }

    public GameBoard(int pawns, int attemps, int colors, boolean repeatColors, GridLayout g) {
        this(pawns, attemps, colors, repeatColors);
        this.panel = new JPanel(g);
        addPawnsToPanel();
    }

    private void initGameBoard() {
        for (int i = 0; i < attemps; i++) {
            for (int j = 0; j < pawns; j++) {
                GameBoardOfPawns[i][j] = new Pawn(colors);
            }
        }
    }

    private boolean contains(int[] array, int element) {
        for (int value : array) {
            if (value == element) {
                return true;
            }
        }
        return false;
    }

    private void initSecretCode() {
        Random rand = new Random();
        for (int i = 0; i < pawns; i++) {
            if (repeatColors) {
                secretCode[i] = rand.nextInt(colors) + 1;
            } else {
                int color = rand.nextInt(colors) + 1;
                while (contains(secretCode, color)) {
                    color = rand.nextInt(colors) + 1;
                }
                secretCode[i] = color;
            }
        }
    }

    private void addPawnsToPanel() {
        for (int i = attemps - 1; i >= 0; i--) {
            for (int j = 0; j < pawns; j++) {
                GameBoardOfPawns[i][j].setButton();
                panel.add(GameBoardOfPawns[i][j].button);
            }
        }
    }

    public void disableClick() {
        for (int i = 0; i < pawns; i++) {
            GameBoardOfPawns[currentAttemp][i].setButtonClickable(false);
        }
    }

    public void addResultToGameBoard(AttempResult result) {
        for (int i = 0; i < result.getCcAp(); i++) {
            GameBoardOfPawns[currentAttemp][i].setIconColor(1);
        }
        for (int i = result.getCcAp(); i < result.getCcAp() + result.getCcWp(); i++) {
            GameBoardOfPawns[currentAttemp][i].setIconColor(0);
        }
    }

    public AttempResult checkRowFeedback(int currentAttemp) {
        int ccap = 0;
        int ccwp = 0;
        boolean[] checkedSecret = new boolean[pawns];
        boolean[] checkedAttempt = new boolean[pawns];

        for (int i = 0; i < pawns; i++) {
            if (GameBoardOfPawns[currentAttemp][i].getCurrentColorIndex() == secretCode[i]) {
                ccap++;
                checkedSecret[i] = true;
                checkedAttempt[i] = true;
            }
        }

        for (int i = 0; i < pawns; i++) {
            if (!checkedSecret[i]) {
                for (int j = 0; j < pawns; j++) {
                    if (!checkedAttempt[j]
                            && GameBoardOfPawns[currentAttemp][j].getCurrentColorIndex() == secretCode[i]) {
                        ccwp++;
                        checkedSecret[i] = true;
                        checkedAttempt[j] = true;
                        break;
                    }
                }
            }
        }

        return new AttempResult(ccwp, ccap);
    }

    public void printGameBoard() {
        for (int i = attemps - 1; i >= 0; i--) {
            for (int j = 0; j < pawns; j++) {
                System.out.print(GameBoardOfPawns[i][j] + "   ");
            }
            System.out.println();
        }
    }

    public void reset() {
        if (panel != null) {
            panel.removeAll();
        }
        initSecretCode();
        currentAttemp = 0;
        initGameBoard();
        if (panel != null) {
            addPawnsToPanel();
        }
    }

    public void updateView() {
        for (int i = 0; i < pawns; i++) {
            GameBoardOfPawns[currentAttemp][i].updatePawn();
        }
    }

    public int submit() {
        AttempResult result = checkRowFeedback(currentAttemp);
        if (currentAttemp < attemps) {
            if (result.getCcAp() == pawns) {
                addResultToGameBoard(checkRowFeedback(currentAttemp));
                updateView();
                return 1;
            } else {
                disableClick();

                addResultToGameBoard(checkRowFeedback(currentAttemp));
                System.out.println("Secret code:");
                for (int i = 0; i < pawns; i++) {
                    System.out.print(secretCode[i] + " ");
                }
                System.out.println();
                printGameBoard();
                return 2;
            }
        } else {
            reset();
            return 0;
        }
    }

    public void addGuess(int[] guess) {
        for (int i = 0; i < pawns; i++) {
            GameBoardOfPawns[currentAttemp][i].setCurrentColorIndex(guess[i]);
        }
    }

    public int getPawns() {
        return pawns;
    }

    public int getAttemps() {
        return attemps;
    }

    public int getColors() {
        return colors;
    }

    public boolean getRepeatColors() {
        return repeatColors;
    }

    public int getCurrentAttemp() {
        return currentAttemp;
    }

    public Pawn[][] getGameBoardOfPawns() {
        Pawn[][] copy = new Pawn[attemps][pawns];
        for (int i = 0; i < attemps; i++) {
            for (int j = 0; j < pawns; j++) {
                copy[i][j] = GameBoardOfPawns[i][j];
            }
        }
        return copy;
    }

    public int[] getSecretCode() {
        int[] copy = new int[pawns];
        for (int i = 0; i < pawns; i++) {
            copy[i] = secretCode[i];
        }
        return copy;
    }

    public void increamentCurrentAttemp() {
        currentAttemp++;
    }

}
