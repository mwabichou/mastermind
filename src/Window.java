
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.border.Border;

public class Window extends JFrame {
    private static final long serialVersionUID = 1L;
    private int gamesPlayed = 0;
    private int gamesWon = 0;
    private int gamesLost = 0;
    private int gamesAborted = 0;
    private int actualPlayer = 0;
    private GameBoard game;
    private Player[] players;
    private JLabel player1;
    private JLabel player2;

    public Window() {
    }

    public Window(GameBoard GameBoard, int width, int height, Player[] players) {
        super("MasterMind | Game");
        GameBoard.updateView();
        revalidate();
        repaint();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double widthRatio = screenSize.getWidth() / 1920;
        double heightRatio = screenSize.getHeight() / 1080;
        width = (int) (width * widthRatio);
        height = (int) (height * heightRatio);
        setSize(width, height);
        setMinimumSize(new Dimension(width, height));
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel topPanel = new JPanel();
        JPanel rightPanel = new JPanel();

        rightPanel.setLayout(null);
        JPanel leftPanel = new JPanel();
        game = GameBoard;
        game.updateView();
        this.players = players;
        Border margin = new EmptyBorder(5, 10, 10, 5);
        game.panel.setBorder(margin);
        JPanel bottomPanel = new JPanel();
        JLabel colorLabel = createInfoLabel("src/img/colors.png");
        colorLabel.setBounds((int) (30 * widthRatio), (int) (30 * heightRatio), (int) (120 * widthRatio),
                (int) (120 * heightRatio));
        rightPanel.add(colorLabel);
        JLabel sizeLabel = createInfoLabel("src/img/size.png");
        sizeLabel.setBounds((int) (160 * widthRatio), (int) (30 * heightRatio), (int) (120 * widthRatio),
                (int) (120 * heightRatio));
        rightPanel.add(sizeLabel);

        JLabel checkLabel = createInfoLabel("src/img/check.png");
        checkLabel.setBounds((int) (30 * widthRatio), (int) (160 * heightRatio), (int) (120 * widthRatio),
                (int) (120 * heightRatio));
        rightPanel.add(checkLabel);
        JLabel clockLabel = createInfoLabel("src/img/clock.png");
        clockLabel.setBounds((int) (160 * widthRatio), (int) (160 * heightRatio), (int) (120 * widthRatio),
                (int) (120 * heightRatio));
        rightPanel.add(clockLabel);
        JLabel logo = createInfoLabel("src/img/MM_logoRose.png");
        logo.setBounds((int) (30 * widthRatio), (int) (300 * heightRatio), (int) (250 * widthRatio),
                (int) (120 * heightRatio));
        rightPanel.add(logo);
        JLabel gameInfo = createInfoLabel("");
        gameInfo.setBounds((int) (30 * widthRatio), (int) (450 * heightRatio), (int) (250 * widthRatio),
                (int) (120 * heightRatio));
        gameInfo.setBackground(Color.white);
        gameInfo.setFont(new Font("Helvetica", Font.BOLD, 20));
        gameInfo.setText(sessionInfo());
        rightPanel.add(gameInfo);

        JButton saveButton = new JButton("Save");
        saveButton.setFont(new Font("Helvetica", Font.BOLD, 30));
        saveButton.setOpaque(true);
        saveButton.setBackground(Color.lightGray);
        saveButton.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        saveButton.setRolloverEnabled(true);
        saveButton.setIcon(new ImageIcon("src/img/save.png"));
        saveButton.setHorizontalTextPosition(SwingConstants.RIGHT);
        saveButton.setBounds((int) (45 * widthRatio), (int) (580 * heightRatio), (int) (220 * widthRatio),
                (int) (70 * heightRatio));
        rightPanel.add(saveButton);

        JButton restartBtn = new JButton("Restart");
        ImageIcon restartIcon = new ImageIcon("src/restart.png");
        restartBtn.setIcon(restartIcon);
        restartBtn.setFont(new Font("Helvetica", Font.BOLD, 30));
        restartBtn.setOpaque(true);
        restartBtn.setBackground(Color.lightGray);
        restartBtn.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        restartBtn.setRolloverEnabled(true);
        restartBtn.setIcon(new ImageIcon("src/img/restart.png"));
        restartBtn.setHorizontalTextPosition(SwingConstants.RIGHT);
        restartBtn.setBounds((int) (45 * widthRatio), (int) (660 * heightRatio), (int) (220 * widthRatio),
                (int) (70 * heightRatio));
        rightPanel.add(restartBtn);
        JButton submiButton = new JButton("Submit");
        submiButton.setFont(new Font("Helvetica", Font.BOLD, 30));
        submiButton.setForeground(Color.white);
        submiButton.setOpaque(true);
        submiButton.setIcon(new ImageIcon("src/img/submit.png"));
        submiButton.setBackground(new Color(252, 105, 203));
        submiButton.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        submiButton.setRolloverEnabled(true);
        submiButton.setHorizontalTextPosition(SwingConstants.RIGHT);
        submiButton.setBounds((int) (45 * widthRatio), (int) (740 * heightRatio), (int) (220 * widthRatio),
                (int) (70 * heightRatio));
        rightPanel.add(submiButton);

        topPanel.setBackground(Color.lightGray);
        rightPanel.setBackground(Color.darkGray);
        leftPanel.setBackground(new Color(2, 128, 144));
        game.panel.setBackground(Color.darkGray);
        bottomPanel.setPreferredSize(new java.awt.Dimension((int) (60 * widthRatio), (int) (30 * heightRatio)));
        bottomPanel.setBackground(Color.darkGray);

        bottomPanel.setLayout(new GridLayout(1, 2));

        if (players.length == 2) {
            this.player1 = new JLabel(players[0].getName());
            player1.setFont(new Font("Helvetica", Font.BOLD, 20));
            player1.setForeground(Color.black);
            player1.setBounds((int) (45 * widthRatio), (int) (800 * heightRatio), (int) (220 * widthRatio),
                    (int) (70 * heightRatio));
            player1.setText("\u2192 " + players[0].getName() + ": " + players[0].getScore());
            rightPanel.add(player1);

            this.player2 = new JLabel(players[1].getName());
            player2.setFont(new Font("Helvetica", Font.BOLD, 20));
            player2.setForeground(Color.black);
            player2.setBounds((int) (180 * widthRatio), (int) (820 * heightRatio), (int) (220 * widthRatio),
                    (int) (70 * heightRatio));
            player2.setText(players[1].getName() + ": " + players[1].getScore());
            rightPanel.add(player2);
        }

        topPanel.setPreferredSize(new java.awt.Dimension((int) (100 * widthRatio), (int) (100 * heightRatio)));
        rightPanel.setPreferredSize(new java.awt.Dimension((int) (300 * widthRatio), (int) (400 * heightRatio)));
        game.panel.setPreferredSize(new java.awt.Dimension((int) (400 * widthRatio), (int) (400 * heightRatio)));

        add(rightPanel, BorderLayout.EAST);
        add(game.panel, BorderLayout.CENTER);
        pack();
        Timer timer = new Timer(1000, new ActionListener() {
            int time = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                time++;
                if (time < 10) {
                    clockLabel.setText("00:0" + time);
                } else if (time < 60) {
                    clockLabel.setText("00:" + time);
                } else if (time < 600) {
                    clockLabel.setText("0" + time / 60 + ":" + time % 60);
                } else {
                    clockLabel.setText(time / 60 + ":" + time % 60);
                }
                colorLabel.setText("0" + game.getColors());
                sizeLabel.setText("0" + game.getPawns() + "X" + game.getAttemps());
                checkLabel.setText(game.getCurrentAttemp() + "/" + game.getAttemps());
                gameInfo.setText(sessionInfo());
                if (players.length == 2) {
                    if (actualPlayer == 0) {
                        player1.setText("\u2192 " + players[0].getName() + ": " + players[0].getScore());
                        player2.setText(players[1].getName() + ": " + players[1].getScore());
                    } else {
                        player1.setText(players[0].getName() + ": " + players[0].getScore());
                        player2.setText("\u2192 " + players[1].getName() + ": " + players[1].getScore());
                    }
                }
            }
        });
        timer.start();
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // SaveGameBoard saver = new SaveGameBoard();
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("MineSweeper | Save");
                fileChooser.setCurrentDirectory(new File("src/save"));
                int userSelection = fileChooser.showSaveDialog(null);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    String path = fileToSave.getAbsolutePath();
                    save(path);
                }
            }
        });
        restartBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                game.reset();
                gamesAborted++;
                gameInfo.setText(sessionInfo());
                game.panel.repaint();
            }

        });

        submiButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int res = game.submit();
                checkWin(res);
                if (res == 2) {
                    game.increamentCurrentAttemp();
                    actualPlayer = (actualPlayer + 1) % players.length;
                }
                if (game.getCurrentAttemp() == game.getAttemps()) {
                    if (res != 1) {
                        lose();
                        gamesLost++;
                        game.reset();
                    }
                }
                if (players[actualPlayer] instanceof AutoPlayer && res == 2) {
                    ((AutoPlayer) players[actualPlayer]).play(game);
                    game.updateView();
                    res = game.submit();
                    game.increamentCurrentAttemp();
                    checkWin(res);
                    actualPlayer = (actualPlayer + 1) % players.length;
                }
                game.updateView();
            }
        });
        setVisible(true);
    }

    private void checkWin(int res) {
        if (res == 1) {
            win();
            players[actualPlayer].incrementScore();
            gamesWon++;
            game.reset();
        } else if (res == 0 || game.getCurrentAttemp() == game.getAttemps()) {
            lose();
            gamesLost++;
            game.reset();
        }
    }

    private static void lose() {
        UIManager.put("OptionPane.background", Color.LIGHT_GRAY);
        UIManager.put("Panel.background", Color.LIGHT_GRAY);
        UIManager.put("OptionPane.informationIcon", new ImageIcon("src/img/bomb3.png"));
        UIManager.put("OptionPane.messageForeground", Color.red);
        JOptionPane.showMessageDialog(null, "You lost...");
    }

    private void win() {
        UIManager.put("OptionPane.background", Color.lightGray);
        UIManager.put("Panel.background", Color.LIGHT_GRAY);
        UIManager.put("OptionPane.informationIcon", new ImageIcon("src/img/hero.png"));
        UIManager.put("OptionPane.messageForeground", new Color(0, 200, 128));
        JOptionPane.showMessageDialog(null, " GG " + players[actualPlayer].getName() + " You win!" + "!");
    }

    private JLabel createInfoLabel(String fileName) {
        JLabel label = new JLabel();
        ImageIcon icon = new ImageIcon(fileName);
        LineBorder border = new LineBorder(Color.black, 3, true);
        label.setIcon(icon);
        label.setFont(new Font("Helvetica", Font.BOLD, 25));
        label.setHorizontalTextPosition(JLabel.CENTER);
        label.setVerticalTextPosition(JLabel.BOTTOM);
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setBorder(border);
        label.setForeground(Color.white);
        return label;
    }

    private String sessionInfo() {
        gamesPlayed = gamesWon + gamesLost + gamesAborted;
        String labelText = "<html>Session Info:<br>Games Played:" + " " + gamesPlayed + "<br>Games Won:" + " "
                + gamesWon + "<br>Games Lost:" + " " + gamesLost + "<br>Games Aborted:" + " " + gamesAborted
                + "</html>";
        return labelText;
    }

    public void save(String path) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path))) {
            out.writeObject(game);
            out.writeInt(actualPlayer);
            out.writeInt(gamesPlayed);
            out.writeInt(gamesWon);
            out.writeInt(gamesLost);
            out.writeInt(gamesAborted);
            out.writeObject(players);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(String path) {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(path))) {
            game = (GameBoard) in.readObject();
            actualPlayer = in.readInt();
            gamesPlayed = in.readInt();
            gamesWon = in.readInt();
            gamesLost = in.readInt();
            gamesAborted = in.readInt();
            players = (Player[]) in.readObject();
            int size[] = MenuUI.screenSize(game.getPawns(), game.getAttemps());
            new Window(game, size[0], size[1], players);
            setActuelPlayer(actualPlayer);
            setGamesPlayed(gamesPlayed);
            setGamesWon(gamesWon);
            setGamesLost(gamesLost);
            setGamesAborted(gamesAborted);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setActuelPlayer(int actualPlayer) {
        this.actualPlayer = actualPlayer;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public void setGamesLost(int gamesLost) {
        this.gamesLost = gamesLost;
    }

    public void setGamesAborted(int gamesAborted) {
        this.gamesAborted = gamesAborted;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }
}
