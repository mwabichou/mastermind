import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AutoPlayer extends Player {
    public AutoPlayer() {
        super("BOT ");
    }

    public void play(GameBoard gameBoard) {
        int[] guess = new int[gameBoard.getPawns()];
        if (gameBoard.getRepeatColors()) {
            for (int i = 0; i < gameBoard.getPawns(); i++) {
                guess[i] = (int) (Math.random() * (gameBoard.getPawns() - 1)) + 1;
                System.out.print(guess[i] + " ");
            }
        } else {
            List<Integer> colors = new ArrayList<>();
            for (int i = 1; i <= gameBoard.getPawns(); i++) {
                colors.add(i);
            }
            Collections.shuffle(colors);
            for (int i = 0; i < gameBoard.getPawns(); i++) {
                guess[i] = colors.get(i);
                System.out.print(guess[i] + " ");
            }
        }
        gameBoard.addGuess(guess);
    }

}
