import java.io.Serializable;

public class Player implements Serializable {
    private String name;
    private int score;

    public Player(String name) {
        this.name = name;
        this.score = 0;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void incrementScore() {
        score++;
    }

    public void decrementScore() {
        score--;
    }

    void play(GameBoard GameBoard) {
    }

    public void addScore(int points) {
        this.score += points;
    }
}