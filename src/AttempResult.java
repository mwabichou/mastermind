public class AttempResult {
    private int CcWp;
    private int CcAp;

    public AttempResult(int CcWp, int CcAp) {
        this.CcWp = CcWp;
        this.CcAp = CcAp;
    }

    public int getCcWp() {
        return CcWp;
    }

    public int getCcAp() {
        return CcAp;
    }

    public void setCcWp(int CcWp) {
        this.CcWp = CcWp;
    }

    public void setCcAp(int CcAp) {
        this.CcAp = CcAp;
    }

}
