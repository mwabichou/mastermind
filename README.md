![Screenshot](src/img/screen.png)

Dans le cadre de notre deuxiemme année de licence en informatique à l'université Clermont Auvergne. Il nous est proposé de réaliser un projet intitulé "Master Mind" en Java.

## UML
![UML](uml/MasterMind.png)
## Pour commencer

Placez vous dans le dossier du projet avec la commande cd

### Pré-requis

Ce qu'il est requis pour lancer le jeu ...

- Java 8 ou plus
- Un IDE (Eclipse, Netbeans, IntelliJ, ...)
- Une machine Linux ou Windows

### Installation

1 - Lancer un terminal dans le dossier du projet
Executez la commande `make run `, cela va compiler et lancer le jeu

2 - Profitez du jeu

## Fabriqué avec

- [Java](https://www.java.com/fr/) - Langage de programmation

- [Swing](https://docs.oracle.com/javase/tutorial/uiswing/) - Bibliothèque graphique

- [make](https://www.gnu.org/software/make/) - Outil de gestion de compilation

## Versions

**Dernière version stable :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.isima.fr/mwabichou/minesweeper/-/tags)

## Auteurs

- **Mohamed Wassim Abichou** _alias_ [@mwabichou](https://gitlab.isima.fr/mwabichou)

## Contributeurs
Lisez la liste des [contributeurs](https://gitlab.isima.fr/mwabichou/minesweeper/-/project_members) pour voir qui à aidé au projet !

## License

Ce projet est sous la licence `MWA`